CC?= cc
CFLAGS+= -std=c99 -pedantic \
	-fno-builtin -O0 \
	-Wall -Wextra -Wall -ggdb
CPPFLAGS+= -D_XOPEN_SOURCE=600
LDFLAGS= -lm

.PHONY: all
all: Makefile runge-kutta plot

sird: sird.o
	$(CC) $(CFLAGS) $(LDFLAGS) sird.o -o sird

runge-kutta: main.o
	$(CC) $(CFLAGS) $(LDFLAGS) main.o -o runge-kutta

clean:
	rm main.o runge-kutta sird data.dat sird.o

.PHONY: plot
plot: sird
	./sird 0.1 600 0.035 0.4 0.005 > data.dat
	gnuplot plot.gp

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@
