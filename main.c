/*
 * runge-kutta - Order 4 Runge-Kutta-Method
 * Copyright (C) 2020  Nico Sonack
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * Here's a function f(x, y) = (x-1)*y^2
 */
static
double f(double x, double y)
{
    return (x - 1.0) * y * y;
}

/*
 * tableau for the 4th order Runge-Kutta method:
 *
 *          0 |   0   0   0   0
 *        0.5 | 0.5   0   0   0
 *        0.5 |   0 0.5   0   0
 *          1 |   0   0   1   0
 *        =====================
 *            | 1/6 1/3 1/3 1/6
 *
 * which produces the following formulas:
 *
 *     k1 = f(x0, y0)
 *     k2 = f(x0 + h/2, y0+h*k1/2)
 *     k3 = f(x0 + h/2, y0+h*k2/2)
 *     k4 = f(x0 + h  , y0 + h*k3)
 *
 *     y1 = y0 + h*(k1 + 2*k2 + 2*k3 + k4) / 6
 */

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;
    double k1, k2, k3, k4, y0, y1, x0, h;

    fprintf(stdout,
            "INFO : Order 4 Runge-Kutta Method\n"
            "     : f(x,y) = (x-1) * y^2      \n");

    /*
     * Start condition: y(0) = 1
     * and we're looking for and approximation of y(0.4)
     * thus: h = x1 - x0 = 0.4 - 0 = 0.4
     */
    y0 = 1;
    x0 = 0;
    h  = 0.4;

    fprintf(stdout,
            "INFO : Starting conditions are:\n"
            "     : x0 = %.5e\n"
            "     : y0 = %.5e\n"
            "     : h  = %.5e\n",
            x0, y0, h);

    k1 = f(x0, y0);
    k2 = f(x0 + 0.5*h, y0 + h * k1 * 0.5);
    k3 = f(x0 + 0.5*h, y0 + h * k2 * 0.5);
    k4 = f(x0 + h    , y0 + h * k3      );

    y1 = y0 + h * (k1 + 2*k2 + 2*k3 + k4) / 6.0;

    fprintf(stdout,
            "INFO : k1 = %.5e\n"
            "     : k2 = %.5e\n"
            "     : k3 = %.5e\n"
            "     : k4 = %.5e\n"
            "     : y1 = %.5e\n",
            k1, k2, k3, k4, y1);

    return EXIT_SUCCESS;
}
