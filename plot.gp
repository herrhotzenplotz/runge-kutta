set term pngcairo size 1280,800
set output 'sir.png'
set xlabel 'Time'
set ylabel '# People'
set title  'SIRD-Model for a pandemic, population size = 4016'
plot "data.dat" using 1 with lines lc rgb '#a33b3b' lw 2 title 'Susceptible', \
     "data.dat" using 2 with lines lc rgb '#a3a68b' lw 2 title 'Infected', \
     "data.dat" using 3 with lines lc rgb '#6d8c9e' lw 2 title 'Recovered', \
     "data.dat" using 4 with lines lc rgb '#3d8c9f' lw 2 title 'Deceased'