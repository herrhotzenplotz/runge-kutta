/*
 * sird - SIRD model
 * Copyright (C) 2020  Nico Sonack
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * SIRD model for simulation of a pandemic
 *
 * S : Susceptible
 * I : Infected/-ious
 * R : Recovered
 * D : Deceased
 *
 * The following system of ODE's:
 *
 *   S' = (-beta / N) * I * S
 *   I' = ( beta / N) * I * S - alpha*I - µ*I
 *   R' = ( alpha   ) * I
 *   D' = µ * I
 *
 * Where
 *   N : Size of the population
 *   a : rate of recovery
 *   b : rate of infection
 *   µ : rate of mortaility
 *
 * A simple Euler-method would be:
 *  (s|i|r|d)'  = F((s|i|r|d)_1, t0)
 *  (s|i|r|d)_2 = (s|i|r|d) + dt * (s|i|r|d)'
 *
 */

static
void usage()
{
    fprintf(stderr,
            "usage: ./sir <dt> <n> <alpha> <beta> <my>\n"
            " arguments:\n"
            "       dt : size of a time step\n"
            "        n : number of steps to compute\n"
            "    alpha : rate of recovery,  put 0.07142 here if you don't know what to do\n"
            "     beta : rate of infection, put 0.333   here if you don't know what to do\n"
            "       my : rate of mortality, put 0.005   here if you don't know what to do\n");
}

static
void vec4_add(const double *a,
              const double *b,
              double *out)
{
    int i;
    for ( i = 0; i < 4; ++i ) {
        out[i] = a[i] + b[i];
    }
}

static
void vec4_mul(const double l,
              const double *x,
              double *out)
{
    int i;
    for ( i = 0; i < 4; ++i ) {
        out[i] = l*x[i];
    }
}

static
void vec4_zeroise(double *foo)
{
    int i;
    for (i = 0; i < 4; ++i) {
        foo[i] = 0.0;
    }
}

/*
 * To make indexing into the vectors a bit more readable
 */
enum VectorIndex {
    IDX_S = 0,
    IDX_I = 1,
    IDX_R = 2,
    IDX_D = 3,
};

/*
 * SIR DE-System
 */
static
void f(const double alpha,
       const double beta,
       const double my,
       const double N,
       const double *s0,
       double *ds)
{
    ds[IDX_S] = (-beta / N) * s0[IDX_I] * s0[IDX_S];
    ds[IDX_I] = ( beta / N) * s0[IDX_I] * s0[IDX_S] - alpha * s0[IDX_I] - my * s0[IDX_I];
    ds[IDX_R] = alpha * s0[IDX_I];
    ds[IDX_D] = my * s0[IDX_I];
}

int main(int argc, char *argv[])
{
    double alpha, beta, my, N, dt, s[4], temp[4];
    int i, n;

    if (argc < 2) {
        fprintf(stderr, "ERR : No arguments provided\n");
        usage();
        return EXIT_FAILURE;
    }

    if (argc != 6) {
        fprintf(stderr,
                "ERR : Invalid number of arguments provided\n"
                "    : Expected 5 but got %d\n", argc - 1);
        usage();
        return EXIT_FAILURE;
    }

    /*
     * Let's parse some data
     */
    if (sscanf(argv[1], "%lf", &dt) != 1) {
        fprintf(stderr,
                "ERR : dt must be a floating point number\n");
        usage();
        return EXIT_FAILURE;
    }

    if (sscanf(argv[3], "%lf", &alpha) != 1) {
        fprintf(stderr,
                "ERR : alpha must be a floating point number\n");
        usage();
        return EXIT_FAILURE;
    }

    if (sscanf(argv[4], "%lf", &beta) != 1) {
        fprintf(stderr,
                "ERR : beta must be a floating point number\n");
        usage();
        return EXIT_FAILURE;
    }

    if (sscanf(argv[5], "%lf", &my) != 1) {
        fprintf(stderr,
                "ERR : my must be a floating point number\n");
        usage();
        return EXIT_FAILURE;
    }

    if (sscanf(argv[2], "%d", &n) != 1) {
        fprintf(stderr,
                "ERR : n must be an integer\n");
        usage();
        return EXIT_FAILURE;
    }

    /* Initial conditions of the model */
    s[IDX_S] = 4015.0;
    s[IDX_I] = 1.0;
    s[IDX_R] = 0.0;
    s[IDX_D] = 0.0;

    /* Size of population */
    N = 4016.0;

    /* Step through the system of DEs */
    for (i = 0; i < n; ++i) {
        vec4_zeroise(temp);
        f(alpha, beta, my, N, s, temp);
        vec4_mul(dt, temp, temp);
        vec4_add(temp, s, s);
        fprintf(stdout, "%lf %lf %lf %lf\n", s[IDX_S], s[IDX_I], s[IDX_R], s[IDX_D]);
    }

    return 0;
}
